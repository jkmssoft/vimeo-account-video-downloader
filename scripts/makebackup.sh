#!/bin/bash

tar -zcvf ../$(basename $(pwd))_$(date "+%Y-%m-%dT%H-%M-%S").tgz  --exclude="vendor" --exclude="testfiles" --exclude="test" .
# todo itself foldername in tar, at the moment it is "."
#tar -zcvf ../$(basename $(pwd))_$(date "+%Y-%m-%dT%H-%M-%S").tgz  --exclude="vendor" ../"$(basename \"$PWD\")"
