# jkmssoft/vimeo-backup
# Name: vimeo-backup
# Network: Bridge
# Storage: Mount Host Pfad, /backup/vimeo => in Container: /backup RW

FROM ubuntu:latest
ENV TZ=Europe/Berlin
ENV DEBIAN_FRONTEND=noninteractive

# install
RUN apt-get update && apt-get -y install wget bash vim openssh-client tzdata cron composer php-cli php-curl

COPY . /root/vimeo-backup/
RUN cd /root/vimeo-backup/ && composer install

# Add the cron job
# https://crontab-generator.org/
RUN crontab -l | { cat; echo "0 13 * * 0 cd /root/vimeo-backup/; php /root/vimeo-backup/VimeoBackup.php >/dev/null 2>&1"; } | crontab -

# backup directory
RUN mkdir /backup
VOLUME /backup

# open port for ssh
# disabled EXPOSE 22

# RUN cron
CMD service cron start ; while true ; do sleep 100; done;
