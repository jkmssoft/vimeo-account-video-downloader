# Vimeo Backup (Vimeo Account Video Downloader)

A php script to backup your own vimeo videos from your account.

## Features
- Incremental backup of your Vimeo videos.
- Replicates the folders from Vimeo local.

## Usage
### Configuration
Create a API key token:
https://developer.vimeo.com/apps
with following scopes: public, private and video files

Copy config.dist.php to config.php

Add this id, secret and token to config.php.
Also add email credentials if you want to get an email after each execution.

### Test
```
php VimeoBackup.php
```

### Cron
Example:
```
0 22 0 * * cd /vimeo-backup/; /usr/local/bin/php /vimeo-backup/VimeoBackup.php
```

## To Do
See VimeoBackup.php

# Keywords
incremental vimeo backup, copy vimeo to local, 
