<?php
/**
 * @category Vimeo-Backup
 * @package  Vimeo-Backup
 * @author   jkmssoft
 * @license  GPL-3.0
 * @link     https://gitlab.com/jkmssoft/vimeo-backup
 */

/*
 * Script to backup all your videos from your PRO vimeo account.
 * Script gets a list of all folders, 
 * and a list of all videos of a specific folder
 * with their download links.
 * Then it downloads folder by folder, incremental.
 * 
 * TODO
 * deleted files? at the moment not handled, local files are not deleted
 * cleanup
 * merge all jsons in one!
 * Problem: links only valid vor 24h, then new generation required
 *      TO DO: download link has expired. Please generate them again. (with get video list per folder)
 * php parameter
 * detect existing, already downloaded file/folders with their id, which is not in the expected folder, if moved online to another folder
 */

require_once __DIR__ . '/vendor/autoload.php';

use Vimeo\Vimeo;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

// check requirements:
if (!extension_loaded('curl')) {
    die('The "curl" extension is required by this connection backend - MISSING! EXIT');
}

/**
 * Exception
 */
class BandwithLimitReachedException extends \Exception
{
}

/**
 * VimeoBackup class.
 */
class VimeoBackup
{
    private $config;
    private $client;
    private $folderStructure = null;
    private $mailContent = [
        'new' => '',
        'log' => '',
        'existing' => '',
    ];

    /** Constructor.
     * @param mixed[] $config Configuration.
     */
    public function __construct($config)
    {
        $this->config = $config;
        $this->client = new Vimeo(VIMEO_CLIENT_ID, VIMEO_CLIENT_SECRET, VIMEO_ACCESS_TOKEN);
        if (!file_exists('datastore')) { // todo var!
            mkdir('datastore', 0755, true);
        }
    }

    /**
     * Execute all.
     * 
     * @return null
     */
    public function run()
    {
        // get the folders
        $this->getFolderList();
        // create the backup folders
        $this->createFolderStructure();
        // download the videos
        try {
            $this->getVideosFromFolderList();
        } catch(BandwithLimitReachedException $e) {
            // catch bandwith limit exception
            $this->log('monthly bandwith limit reached - STOP!', 'log');
        }

        if (!empty($this->config['mail']['password'])) {
            $subject = '';
            
            $content = 'Vimeo Backup Output:<br>'
                .'ERROR:<br>'.$this->mailContent['error']
                .'<br><br>New Videos:<br>'.$this->mailContent['new']
                .'<br><br>Log:<br>'.$this->mailContent['log']
            ;
            
            $this->sendMail($content, $subject);
        }
    }

    /**
     * Get list of all folders from vimeo.
     */
    public function getFolderList()
    {
        // get folder list
        echo 'get folder list from vimeo...';
        $page = 1;
        $perPage = 100;    
        $folderList = []; // projects = folders
        $end = false;
        $folderCount = 0;
        while ( $end == false ) {
            if (USE_API) {
                // execute this for all pages until there is a empty page
                $foldersRequest = $this->client->request(
                    '/me/projects',
                    array(
                        'page'     => $page,
                        'per_page' => $perPage,
                    )
                );
                // store it:
                //disabled file_put_contents('datastore/vimeo_meprojects.json', json_encode($foldersRequest)); // create file for testing
                //exit;
            } else {
                $foldersRequest = json_decode(file_get_contents('datastore/vimeo_meprojects.json'), true);
            }
            $folderTotal = $foldersRequest['body']['total'];
            $folders = $foldersRequest['body']['data'];
            if (! is_array($folders) ) {
                $end = true;
                break;
            }
            if (count($folders) == 0 ) {
                $end = true;
            }
            
            foreach ( $folders as $folder ) {
                $folderCount++;
                $data = [];
                $data['created_time'] = $folder['created_time'];
                $data['uri'] = $folder['uri'];
                $data['id'] = array_slice(explode('/', $folder['uri']), -1)[0];
                $data['name'] = $data['id'].'_'.self::getFilesystemValidName($folder['name'], 80); // here folder gets id prefixed in name
                $data['ancestor'] = [];
                $data['children'] = [];
                
                $parentFolderName = ''; // reset
                foreach ($folder['metadata']['connections']['ancestor_path'] as $index => $ancestor ) {
                    $data['ancestor'][] = array_slice(explode('/', $ancestor['uri']), -1)[0];
                }
                // if not yet in folder list then add folder
                if (! array_key_exists($data['id'], $folderList) ) {
                    $folderList[$data['id']] = $data;
                }
            }
            $page++;

            // only one page?
            if ($folderTotal <= $perPage) {
                $end = true;
            }
        } // while
        
        $this->log('found '.$folderCount.' folders', 'log');

        file_put_contents('datastore/vimeo_folderlist.json', json_encode($folderList));
    } // getFolderList

    /**
     * Get the folder structure as a tree.
     * Store also in class var $folderStructure.
     */
    public function getFolderStructure()
    {
        if (!is_null($this->folderStructure)) {
            return $this->folderStructure;
        }
        $folderVideoList = json_decode(file_get_contents('datastore/vimeo_folderlist.json'), true);       
        // make array folder tree list
        $treeFolderVideoList = self::buildTree($folderVideoList);
        // get folder structure for mkdir
        $folderStructure = []; // /a/b/c, ...
        // create the folder list
        $folderStructure = $this->createFolderList($treeFolderVideoList, '');
        
        $this->folderStructure = $folderStructure;
        
        return $folderStructure;
    }

    /**
     * Log and echo outputs.
     */
    protected function log($str, $type = 'log')
    {
        echo $str.PHP_EOL;
        $this->mailContent[$type] .= $str.'<br>';        
    }

    /**
     * Create the folders structure with mkdir.
     */
    public function createFolderStructure()
    {       
        $folderStructure = $this->getFolderStructure();
        
        // create the folders
        foreach ($folderStructure as $folder) {
            $folder = BACKUP_FOLDER.$folder;
            $this->log('mkdir("'.$folder.'", 0755, true)', 'log');
            
            if (!DRY_RUN) {
                if (!file_exists($folder)) {
                    mkdir($folder, 0755, true);
                }
            }
        }
    } // createFolderStructure

    /**
     * Get all videos from each folder and download them.
     */
    public function getVideosFromFolderList()
    {
        // get video list per folder
        $folderList = json_decode(file_get_contents('datastore/vimeo_folderlist.json'), true);
        $folderVideoList = $folderList;

        foreach ( $folderVideoList as $folderUri => &$folder ) {
            $this->checkBandwithLimit(0);

            echo 'process folder '.self::getFilesystemValidName($folder['name'], 80).' '.$folder['uri'].'/videos'.PHP_EOL;
            
            // get videos list from this folder:
            // todo if during process urls expire, then this needs to be executed again:
            $videos = $this->getVideoListFromFolder($folder);

            // download this folders videos:
            foreach ($videos as $video) {
                $this->downloadVideo($folder, $video);
            }
            // todo remove exit! for test download only first folder!
            exit;
        } // foreach
               
        // write to json folders and videos
        //disabled file_put_contents('datastore/vimeo_foldervideolist.json', json_encode($folderVideoList));
    } // getVideosFromFolderList

    /**
     * Get the download links for all videos from that folder.
     */
    public function getVideoListFromFolder($folder)
    {
        $page = 1;
        $end = false;
        while ( $end == false ) {
            if (USE_API) {
                $videosRequest = $this->client->request(
                    $folder['uri'].'/videos',
                    array(
                        'page'     => $page,
                        'per_page' => 100,
                    )
                );
                //disabled file_put_contents('datastore/vimeo_videolistfromfolder_'.$folder['name'].'.json', json_encode($videosRequest));
            } else {
                $videosRequest = json_decode(file_get_contents('datastore/vimeo_videolistfromfolder_'.$folder['name'].'.json'), true);
            }
            $videos = $videosRequest['body']['data'];
            // execute this for all pages until there is a empty page
            if (!is_array($videos)) {
                $end = true;
                break;
            }
            if (count($videos) == 0 || count($videos) <= 100) {
                $end = true;
            }
            
            $videosResult = [];

            foreach ($videos as $video) {
                //print_r($video);exit;
                $videoData = [];
                $videoData['name'] = $video['name'];
                $videoData['uri'] = $video['uri'];
                $videoData['id'] = array_slice(explode('/', $video['uri']), -1)[0];
                $dl = $this->getDownload($video);
                if (is_null($dl)) {
                    $this->log('no download link available for video: '.$videoData['name'], 'error');
                }
                $videoData = array_merge($videoData, $dl);
                    
                if (!array_key_exists($video['uri'], $videosResult)) {
                    $videoData['uri'] = $video['uri'];
                    $videosResult[$video['uri']] = $videoData;
                }
            } // foreach
            $page++;
        } // while
        //file_put_contents('datastore/vimeo_getVideoListFromFolder_'.$folder['id'].'.json', json_encode($videosResult));
        return $videosResult;
    } // getVideoListFromFolder

    protected function getDownload($video)
    {
        $videoData = [];
        foreach (['source', 'original', 'hd'] as $quality) {
            foreach ($video['download'] as $download) {
                if ($download['quality'] == $quality) { // found!
                    $videoData['downloadLink'] = $download['link'];
                    $videoData['size'] = $download['size'];
                    $videoData['created_time'] = $download['created_time'];
                    return $videoData;
                }
            }
        }
        return null;
    }    

    /**
     * Extract the filename from the url.
     * @param string $url
     * @return string The filename
     */
    protected function extractFilename($url)
    {
        $filename = explode('?', $url);
        $filename = explode('/', $filename[0]);
        return $filename[9]; // 10. element / is the filename
    }

    /**
     * Extract expires from $url and return it.
     * Returns on error -1.
     */
    protected function getExpires($url)
    {
        if (preg_match('#expires=([\d]+)#', $url, $expires) !== false) {
            return $expires[1];
        }
        return -1;
    }
    
    /**
     * Download that video from folder.
     */
    protected function downloadVideo($folder, $video)
    {
        if (isset($video['downloadLink'])) {
            $folderStructure = $this->getFolderStructure();
            $folderPath = $folderStructure[$folder['id']]; // todo use treeFolderVideoList
            // downloadlink contains also valid filename
            $filename = $this->extractFilename($video['downloadLink']);
            // todo alternative:  todo add file extension!
            //$filename = $video['name'];

            // allow only allowed characters for filesystem
            // prefix the video id
            $filename = $video['id'].'_'.self::getFilesystemValidName($video['created_time'].'_'.urldecode($filename), null); // null=no length limit

            // full path with name 
            $filePathName = $folderPath.DIRECTORY_SEPARATOR.$filename;

            // todo make this better:
            $str = "try download: ".BACKUP_FOLDER.$filePathName."...";
            echo $str;
            $this->mailContent['new'] .= $str;

            // detect expires
            $expires = $this->getExpires($video['downloadLink']);
            if ($expires > 0) {
                if ($expires - time() < 1800) {
                    $this->log('download link expires soon ('.$expires.'), not downloading!', 'new');
                    // todo now reload the folder urls!
                    return;
                }
            }        

            // if local file exists
            if (file_exists($filePathName)) {
                // check filesize
                if ($video['size'] == filesize($filePathName)) {
                    // if same size go to next
                    $this->log($filePathName.': download not necessary, file with same name and size already exists!', 'log');
                    return;
                } else {
                    // if file is only partial there then we do nothing here and wget will continue download automatically.
                }
                // todo check create date?
            } else {
                // todo search for that filename everywhere
                // todo check size of found file
                // could be same file, only moved!
            }

            //                                  byte / kbyte  / mbyte  / gbyte
            $this->checkBandwithLimit($video['size'] / 1024.0 / 1024.0 / 1024.0);

            if (!DRY_RUN) {
                if (true) {
                    $this->log('execute download...', 'new');
                    // download with wget
                    // without -c because this could be a problem
                    // but without -c broken download is not handled!
                    $output = shell_exec('wget -O "'.BACKUP_FOLDER.$filePathName.'" "'.$video['downloadLink'].'"');
                    print_r($output);
                    $this->mailContent['new'] .= $output;
                }
            } else {
                echo 'Dry Run - do not download!';
            }
            $str = "done.\n";
            $this->log($str, 'new');
        }
    } // downloadVideo

    protected function findVideo($folder, $video)
    {
        // todo find and handle moves of videos/folders
    }
    
    /**
     * Create a folder list.
     */
    protected function createFolderList(&$treeFolderVideoList, $parent)
    {
        $folders = [];
        foreach ($treeFolderVideoList as &$folder) {
            if (count($folder['children']) > 0) {
                $folders = array_replace(
                    $folders,
                    $this->createFolderList($folder['children'], $folder['name'])
                ); 
            } else {
                $folders[$folder['id']] = ($parent == '' ? '' : $parent.DIRECTORY_SEPARATOR).$folder['name'];
            }
        }
        return $folders;
    }
    
    /**
     * Check bandwith limit per month.
     * Throws an exception if over limit.
     */
    protected function checkBandwithLimit($gb)
    {
        if (file_exists('datastore/bandwithData.json')) {
            $bandwithData = json_decode(file_get_contents('datastore/bandwithData.json'), true);
        } else {
            $bandwithData = [];
        }
        $thisMonth = date('Y-m');
        
        if (!isset($bandwithData[$thisMonth])) {
            $bandwithData[$thisMonth] = 0;
        }
        
        if ($bandwithData[$thisMonth] + $gb > BANDWITH_LIMIT_MONTH_GB) {
            $result = false;
        } else {
            $bandwithData[$thisMonth] += $gb;
            $result = true;
        }
        
        file_put_contents('datastore/bandwithData.json', json_encode($bandwithData));
        
        if (!$result) {
            throw new \BandwithLimitReachedException('monthlyLimitReached - STOP');
        }
    }

    /**
     * Send an email.
     *
     * @return null
     */
    protected function sendMail($message, $subject = '')
    {
        $mail = new PHPMailer(true);

        try {
            // Server settings
            $mail->SMTPDebug = false;// SMTP::DEBUG_SERVER;
            $mail->isSMTP();
            $mail->Host       = $this->config['mail']['host']; 
            $mail->SMTPAuth   = true;
            $mail->Username   = $this->config['mail']['username'];
            $mail->Password   = $this->config['mail']['password'];
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS; // Enable implicit TLS encryption
            $mail->Port       = $this->config['mail']['port']; //TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`

            // Recipients
            $mail->setFrom($this->config['mail']['fromEmail'], $this->config['mail']['fromEmail']);
            $mail->addAddress($this->config['mail']['toEmail'], $this->config['mail']['toEmail']);

            // Content
            $mail->isHTML(true);
            $mail->Subject = 'Vimeo Backup '.$subject;
            $mail->Body = $message;
            //$mail->AltBody = '';

            $mail->send();
            
            $this->log('E-Mail has been sent to '.$this->config['mail']['toEmail'].'.', 'log');
        } catch (Exception $e) {
            $this->log("E-Mail could not be sent. Mailer Error: {$mail->ErrorInfo}", 'log');
        }
    }

    /**
     * Return a valid file/folder name from given $string.
     *
     * @param  string   $string The string to clean.
     * @param  int|null $length Limit length to $length.
     * @return valid file/folder name from given $string.
     */
    protected static function getFilesystemValidName($string, $length = null)
    {
        $string = trim($string);
        $string = preg_replace("/[\\/?%*:|\"<>]+/", '-', $string); // https://stackoverflow.com/questions/12680061/check-a-string-for-a-valid-directory-name
        if (!is_null($length)) {
            return substr($string, 0, $length);
        }
        return $string;
    }
    //teststring echo VimeoBackup::getFilesystemValidName("filename/Foldername: a?a%a*a:a|a\"a<a>a");
    
    /**
     * Build the folder structure.
     */
    protected static function buildTree(array $elements, $parentId = 0)
    {
        $branch = [];

        foreach ($elements as $element) {
            if (!isset($element['ancestor'][0])) {
                $element['ancestor'][0] = 0; // 0 as root
            }
            if ($element['ancestor'][0] == $parentId) {
                $children = self::buildTree($elements, $element['id']);
                if ($children) {
                    $element['children'] = $children;
                }
                $branch[] = $element;
            }
        }

        return $branch;
    } // buildTree
}

// script it's self called and not included
if (isset($argv[0]) && realpath($argv[0]) == realpath(__FILE__)) {
    include_once 'config.php'; // $config
    $p = new VimeoBackup($config);
    $p->run();
    exit;
}
