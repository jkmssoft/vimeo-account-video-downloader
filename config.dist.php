<?php

// scopes to select for API key generation in vimeo: public private video_files
define('VIMEO_CLIENT_ID', '');
define('VIMEO_CLIENT_SECRET', '');
define('VIMEO_ACCESS_TOKEN', '');
define('BANDWITH_LIMIT_MONTH_GB', 500); // I don't know if api downloads count to vimeo bandwith limit! See https://vimeo.com/analytics/bandwith
define('BACKUP_FOLDER', '/backup/');
define('DRY_RUN', true); // simulate only
define('USE_API', true);

$config['mail']['host'] = '';
$config['mail']['username'] = '';
$config['mail']['password'] = ''; // keep empty to disable
$config['mail']['port'] = '';
$config['mail']['fromEmail'] = '';
$config['mail']['toEmail'] = '';
